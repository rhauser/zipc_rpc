A simple RPC framework using msgpack on top of the 'zipc' package and ZeroMQ.

Setup
=====

```bash
source /afs/cern.ch/atlas/project/tdaq/cmt/bin/cmtsetup.sh tdaq-common-01-42-00
source /afs/cern.ch/work/r/rhauser/public/ipc-ng/ipc-ng-00-00-01/installed/setup.sh
export CMTPROJECTPATH=${CMTPROJECTPATH}:/afs/cern.ch/work/r/rhauser/public

mkdir -p work/cmt 
cd work/cmt
cat > project.cmt <<EOF
project work

use ipc-ng ipc-ng-00-00-01
EOF
```

Usage
=====

Look at the `test` directory of the `zipc_rpc` package for a server and client example, both in C++ and Python.

C++
---

```bash
zipc_server &
export IPC_INIT_REF=...address that zipc_server just printed...
test_server &
test_listener &
test_client
zipc_ls
```

Python
------

```bash
cd zipc_rpc/python
export PYTHONPATH=${TDAQ_PYTHONPATH}:`pwd`
./test/test_server.py &
./test/test_client.py
```


