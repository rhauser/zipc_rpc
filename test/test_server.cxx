

#include <unistd.h>
#include <iostream>

#include "ipc/Partition.hpp"
#include "zipc_rpc/Server.h"

#include "MyData.h"
#include "ers/SampleIssues.h"

// A stand-alone function to be exposed
std::string standalone(const std::string& input)
{
    return "This is standalone, returning: " + input;
}

//
// A class that inherits from Server and
// registers its own methods with std::bind
//
class MyServer : public tdaq::rpc::Server {
public:
    MyServer()
        : tdaq::rpc::Server(zmq::socket_type::rep)
   {
       tdaq::ipc::Partition partition;
       partition.publish(*this, "rpc_server");

       add_method("echo", &MyServer::echo, this, std::placeholders::_1);
       add_method("add", &MyServer::add, this, std::placeholders::_1, std::placeholders::_2);
       add_method("add_10", &MyServer::add, this, std::placeholders::_1, 10);
   }

   std::string echo(const std::string& msg)
   {
       return "This is MyServer::echo, returning: " + msg;
   }

    int add(int x1, int x2)
    {
        return x1 + x2;
    }
};

class Listener : tdaq::rpc::OnewayServer {
public:
    Listener()
        : OnewayServer(zmq::socket_type::dealer)
    {
        add_method("notify_me", &Listener::notify, this, std::placeholders::_1);
        tdaq::ipc::Partition partition;
        partition.publish(*this, "rpc_listener");
    }

private:
    void notify(const std::string& message)
    {
        std::cout << "Hey, Listener got a notification: " << message << std::endl;
    }
};

// Another class that does not inherit from Server.
// The method can still be exposed, 
class OtherService {
public:
   std::string more_echo(const std::string& msg)
   {
     return "This is OtherService::more_echo, returning: " + msg;
   }
};

// A callable object
struct Functor {
  

  std::string operator()(const std::string& msg) const
  {
      return "This is Functor::operator() returning: " + msg;
  } 

};

//
//  combinations
//

bool test_0()
{
    return true;
}

// void return type
void test_1()
{
    // do something
}

int test_2(const std::string& arg1, float f)
{
    (void )arg1;
    (void )f;
    return 123;
}

double test_3(const std::string& arg1, int arg2, float arg3)
{
    (void )arg1;
    (void )arg2;
    (void )arg3;    
    return 3.141;
}

// non-trivial data types
size_t test_array(const std::vector<int>& data)
{
    return data.size();
}

size_t test_map(const std::map<std::string, int>& data)
{
    return data.size();
}

int test_user(const MyData& data)
{
    return data.x;
}

int test_user_array(const std::vector<MyData>& data)
{
    return data.size();
}

float test_user2(const MyData2& data)
{
    return data.f;
}

int test_stdex()
{
    throw std::runtime_error("Just an example for a std::exception");
}

void test_issue()
{
    throw ers::CantOpenFile(ERS_HERE, "myfilename.root");
}

int main()
{
    OtherService service2;
    Functor func;

    MyServer server;
    Listener listener;

    std::cout << server.getEndpoint().getAddress() << std::endl;

    server.add_method("my_echo", standalone);
    server.add_method("more_echo", &OtherService::more_echo, &service2, std::placeholders::_1);
    server.add_method("functor", func);
    server.add_method("lambda", [](const std::string& msg) -> std::string { return "This is lambda, returning: " + msg; });

    server.add_method("test_0", test_0);
    server.add_method("test_1", test_1);
    server.add_method("test_2", test_2);
    server.add_method("test_3", test_3);    
    server.add_method("test_array", test_array);    
    server.add_method("test_map", test_map);

    server.add_method("test_user", test_user);
    server.add_method("test_user_array", test_user_array);
    server.add_method("test_user2", test_user2);

    server.add_method("test_stdex", test_stdex);
    server.add_method("test_issue", test_issue);

    while(true) {
        pause();
    }

}
