
#include "msgpack.hpp"
#include <iostream>
#include "zipc_rpc/Client.h"

#include "MyData.h"
#include "ers/SampleIssues.h"

int main(int argc, char *argv[])

{
    using tdaq::rpc::Client;
    using tdaq::rpc::PublicClient;

    tdaq::ipc::Partition partition; 

    tdaq::ipc::Endpoint ep = argc == 2 ? tdaq::ipc::Endpoint::fromString(argv[1]) : partition.lookup("rpc_server");

    Client client(zmq::socket_type::req, ep);

    auto r = client.call<std::string>("echo", "Hi there");
    std::cout << "Result = " << r << std::endl;

    r = client.call<std::string>("my_echo", "Hi there");
    std::cout << "Result = " << r << std::endl;

    r = client.call<std::string>("more_echo", "Hi there");
    std::cout << "Result = " << r << std::endl;

    r = client.call<std::string>("functor", "Hi there");
    std::cout << "Result = " << r << std::endl;

    r = client.call<std::string>("lambda", "Hi there");
    std::cout << "Result = " << r << std::endl;

    auto sum = client.call<int>("add", 10, 23);
    std::cout << "Result add = " << sum << std::endl;

    sum = client.call<int>("add_10", 10);
    std::cout << "Result add_10 = " << sum << std::endl;

    std::vector<int> a{1,2,3,4,5};
    size_t s = client.call<size_t>("test_array", a);
    std::cout << "Size of array = " << s << std::endl;

    std::map<std::string, int> m{ {"hello", 10}, { "world", 20} };
    std::cout << "Size of map = " << client.call<size_t>("test_map", m) << std::endl;

    MyData data{10, 3.141, "Hello world"};
    std::cout << "X of user data = " << client.call<int>("test_user", data) << std::endl;

    std::vector<MyData> data_array{{10, 3.141, "Hello world"}, {20, 6.28, "Hi there" }};
    std::cout << "Size of user data array = " << client.call<int>("test_user_array", data_array) << std::endl;    

    MyData2 data2{10, 3.141, "Hello world"};
    std::cout << "F of user data = " << client.call<float>("test_user2", data2) << std::endl;

    auto f = client.async_call<std::string>("echo", "A future test");
    auto future_result = f.get();
    std::cout << "Result of async_call: " << future_result << std::endl;

    // call with no return value
    client.call<void>("test_0");
    std::cout << "test_0 successfully called, no return value" << std::endl;

    // call with no return value
    client.call<void>("test_1");
    std::cout << "test_1 successfully called, no return value" << std::endl;

    try {
        client.call<int>("test_stdex");
    } catch (std::runtime_error& ex) {
        std::cout << "Caught remote runtime_error: " << ex.what() << std::endl;
    } catch (std::exception& ex) {
        std::cout << "Caught remote std::exception: " << ex.what() << std::endl;
    }

    try {
        client.call<void>("test_issue");
    } catch (ers::File& ex) {
        std::cout << ex.what() << std::endl;
    } catch (ers::Issue& ex) {
        std::cout << ex.what() << std::endl;
    }

    Client sender(zmq::socket_type::dealer, partition.lookup("rpc_listener"));
    sender.notify("notify_me", "This is a one-way message");

    PublicClient pc(zmq::socket_type::req, partition, "rpc_server");
    std::cout << "Public client call: " << pc.call<std::string>("echo", "Echo public client") << std::endl;
}
