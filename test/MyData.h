// this is -*- c++ -*-
#ifndef ZIPC_RPC_MYDATA_H_
#define ZIPC_RPC_MYDATA_H_

#include <string>

struct MyData {
    int x;
    float f;
    std::string s;
    MSGPACK_DEFINE(x,f,s)
};

// non-intrusive conversion
struct MyData2 {
    int x;
    float f;
    std::string s;
};

namespace msgpack {
    MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
        namespace adaptor {
            
            template <>
            struct pack<MyData2> {
                template <typename Stream>
                msgpack::packer<Stream>& operator()(msgpack::packer<Stream>& s, const MyData2& data) const
                {
                    s.pack_array(3);
                    s.pack(data.x);
                    s.pack(data.f);
                    s.pack(data.s);
                    return s;
                }
            };

            template<>
            struct convert<MyData2> {
                msgpack::object const& operator()(msgpack::object const& o, MyData2& v) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 3) throw msgpack::type_error();
                    v.x = o.via.array.ptr[0].as<int>();
                    v.f = o.via.array.ptr[1].as<float>();
                    v.s = o.via.array.ptr[2].as<std::string>();
                    return o;
                }
            };


        }
    }
}

#endif // ZIPC_RPC_MYDATA_H_
