#!/usr/bin/env tdaq_python

from zipc_rpc import Server, Partition

class MyServer:

    def echo(self, msg):

        return "From python server: " + msg

    def add(self, x, y):
        return x + y

    def test_array(self, array):
        return len(array)

    def test_map(self, data):
        return len(data)

def standalone(msg):
    return "From standalone: " + msg

def functor(msg):
    return "From 'functor': " + msg

srv = Server(server = MyServer())

srv.add_method(standalone, "my_echo")
srv.add_method(functor)

partition = Partition()
partition.publish(srv.endpoint, "rpc_server")

srv.run()
