#!/usr/bin/env tdaq_python

import sys
import zmq
from zipc_rpc import Client, Partition

partition = Partition()

c = Client(len(sys.argv) == 2 and sys.argv[1] or partition.lookup('rpc_server'))

if c.__ping():
    print "Client is alive"

print c.echo("Hello world")
print c.my_echo("Hello world")
print c.functor("Hello world")
print c.add(11,31)
print c.test_array([1,2,3,4,5])
print c.test_map({'test': 10, 'hello': 20})

try:
    c.test_stdex()
except Exception, ex:
    print ex

try:
    c.test_issue()
except Exception, ex:
    print ex

print "Available methods on client: ", c.__list()

try:
    l = Client(partition.lookup('rpc_listener'), zmq.DEALER)
    l.notify("notify_me", "This is a notice from Python")
except Exception, ex:
    print "Got exception in calling notify():", ex
