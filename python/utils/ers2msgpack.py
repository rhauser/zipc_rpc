#!/usr/bin/env tdaq_python

from umsgpack import packb, unpackb

import ers

def convertContext2dict(ctx):
    result = {}
    for attr in [ 'application_name', 'cwd', 'file_name', 'function_name', 'host_name', 'line_number', 'package_name', 'process_id', 'thread_id', 'user_id', 'user_name' ]:
        result[attr] = getattr(ctx, attr)
    return result

def convert2dict(ex):
    result = {}
    result['context'] = convertContext2dict(ex.context)
    result['message'] = ex.message
    result['parameters'] = ex.parameters
    result['qualifiers'] = ex.qualifiers
    result['severity'] = ex.severity
    result['time'] = ex.time
    result['cause'] = ex.cause and convert2dict(ex.cause) or None
    return result

def convert2msgpack(ex):
    return packb(convert2dict(ex))

if __name__ == '__main__':
    import sys
    from pprint import pprint

    ex = ers.Issue('Another error', { 'severity': ers.Severity.WARNING, 'qualifiers': [ 'TDAQ', 'HLT' ]}, None)

    pprint(convert2dict(ex))
    pprint(unpackb(convert2msgpack(ex)))

