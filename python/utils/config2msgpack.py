#!/usr/bin/env tdaq_python

import config
from umsgpack import packb, unpackb

def convert2dict(config_object, follow_relations = 0):
    '''
    Convert an OKS config object to a dictionary.

    The name of the object is mapped to _id, the class
    name to _class.
    
    The follow_relations parameter indicates how
    many levels of references it should follow.
    
    The config_object parameter can be None, in which
    case None is returned.
    '''

    if config_object is None:
        return None

    result = { '_id' : config_object.UID(),
               '_class': config_object.class_name()
               }

    for attr in config_object.__schema__['attribute']:
        result[attr] = config_object[attr]

    if follow_relations > 0:
        for rel in config_object.__schema__['relation']:
            relation = config_object[rel]
            if relation is None:
                result[rel] = None
            elif type(relation) == list:
                result[rel] = []
                for r in relation:
                    result[rel].append(convert2dict(r, follow_relations - 1))
            else:
                result[rel] = convert2dict(relation, follow_relations - 1)
    else:
        # add the names of all relations, so the client can follow them
        # manually if desired
        r = {}
        for rel in config_object.__schema__['relation']:
            if config_object.__schema__['relation'][rel]['multivalue']:
                r[rel] = [ o and o.UID() or None for o in config_object.get_objs(rel) ]
            else:
                r[rel] = config_object.get_obj(rel) and config_object.get_obj(rel).UID() or None
        result['_relations'] = r

    return result

def convert2msgpack(config_object, follow_relations = 0):
   return packb(convert2dict(config_object, follow_relations))

if __name__ == '__main__':
    import sys, os
    from pprint import pprint

    db = config.Configuration(os.environ.get('TDAQ_DB','oksconfig:test.data.xml'))
    if len(sys.argv) == 3:
        obj = db.get_obj(sys.argv[1], sys.argv[2])
    else:
        obj = db.get_obj('Partition','test')
    pprint(convert2dict(obj,1))
    pprint(unpackb(convert2msgpack(obj,1)))

