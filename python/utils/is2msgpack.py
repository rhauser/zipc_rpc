#!/usr/bin/env tdaq_python

from umsgpack import packb, unpackb

from ispy import *

def convert_value(value, attr_type):
    if attr_type == Basic.ISInfo:
        return convert2dict('',value)
    elif attr_type in [ Basic.String, Basic.Time, Basic.Date ]:
        return str(value)
    else:
        return value

def convert2dict(name, obj):
    result = {}
    result['_id'] = name
    result['_type'] = obj.type().name()
    result['_time'] = str(obj.time())

    for i,n in enumerate(obj.keys()):
        if obj.isAttributeArray(i):
            result[n] = [ convert_value(o, obj.getAttributeType(i)) for o in obj[i] ]
        else:
            result[n] = convert_value(obj[i], obj.getAttributeType(i))

    return result

def convert2msgpack(name, obj):
    return packb(convert2dict(name, obj))

def update_is(obj, data):
    '''
    obj is an ISObject
    data is the attributes as a python dictionary
    returns: the ISObject with the attributes updated from the dictionary.
    '''
    for i in range(obj.getAttributesNumber()):
        attr = obj.getAttributeDescription(i)
        name = attr.name()
        if data.has_key(name):
            try:
                d = data[name]
                if type(d) == unicode:
                    d = str(d)
                if attr.typeCode() == Basic.Date:
                    d = OWLDate(d)
                elif attr.typeCode() == Basic.Time:
                    d = OWLTime(d)
                obj.setAttributeValue(i, d)
            except Exception as ex:
                print ex
    return obj
            

if __name__ == '__main__':
    import sys
    from pprint import pprint

    part = IPCPartition('test')
    name = len(sys.argv) == 1 and 'RunParams.RunParams' or sys.argv[1]
    obj = ISObject(part, name)
    obj.checkout()
    pprint(convert2dict(name, obj))
    pprint(unpackb(convert2msgpack(name, obj)))

