
import zmq
from umsgpack import packb, unpackb

class ClientProxyMethod:
    
    def __init__(self, client, method):
        self.client = client
        self.method = method

    def __call__(self, *args, **kwargs):
        return self.client._do_request(self.method, args, kwargs)

class Client:

    def __init__(self, endpoint, socket_type=zmq.REQ):
        self._transport = zmq.Context.instance().socket(socket_type)
        self._transport.connect(endpoint)
        self._xid = 1
        
    def _do_request(self, method, args, kwargs):
        # kwargs is used for per-call meta information, e.g. timeouts etc.
        # ignored for now
        xid = self._xid
        self._xid += 1
        self._transport.send(packb([0, xid, method, args or []]))
        reply = unpackb(self._transport.recv())
        if reply[3]:
            raise Exception("Error: " + str(reply[3][1]))
        else:
            return reply[2]

    def notify(self, method, *args, **kwargs):
        self._transport.send(packb([2, method, args or []]))
        
    def __getattr__(self, method):
        return ClientProxyMethod(self, method)

