
import os
import zmq

class Partition:

    def __init__(self, context=None):
        self.context = context or zmq.Context.instance()
        self.socket  = self.context.socket(zmq.REQ)
        self.socket.connect(os.getenv('IPC_INIT_REF'))

    def publish(self, endpoint, name):
        self.socket.send('publish ' + name + ' ' + endpoint)
        self.socket.recv()
            
    def lookup(self, name):
        self.socket.send('resolve ' + name)
        return self.socket.recv()
        
    def withdraw(self, name):
        self.socket.send('withdraw ' + name)
        self.socket.recv()

