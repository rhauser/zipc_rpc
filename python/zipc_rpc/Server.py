
import socket
import zmq
from umsgpack import packb, unpackb

class Server(object):

    def __init__(self, server=None, socket_type=zmq.REP, methods={}):
        super(Server, self).__init__()
        self.transport = zmq.Context.instance().socket(socket_type)
        self.transport.bind('tcp://0.0.0.0:*')
        ep = self.transport.last_endpoint
        self.endpoint = 'tcp://' + socket.gethostbyname(socket.gethostname()) + ':' + ep[ep.rindex(':')+1:]
        self.server = server
        self.methods = methods

    def add_method(self, method, name=None):
        if not name:
            name = method.__name__
        self.methods[name] = method

    def run(self):

        self.do_stop = False
        
        while not self.do_stop:

            try:
                request = unpackb(self.transport.recv())
            except KeyboardInterrupt:
                self.do_stop = True
                break
            except Exception as ex:
                print "Exception in server: ",ex
                # note: REP socket will no longer work if something goes wrong here
                continue

            # distinguish RPC and notifications
            if len(request) == 3:
                xid = None
                method = request[1]
                args   = request[2]
            elif len(request) == 4:
                xid    = request[1]
                method = request[2]
                args   = request[3]

            if hasattr(self.server,method):
                func = getattr(self.server,method)
            elif self.methods.has_key(method):
                func = self.methods[method]
            elif method == '__ping':
                func = lambda *args: True
            elif method == '__list':
                func = lambda *args: self.methods.keys() + [ m for m in dir(self.server) if not m.startswith('_') ]
            else:
                print "Unknown method:", method
                if not xid is None:
                    self.transport.send(packb([1, xid, None, "No such method: " + method]))
                continue
            
            try:
                result = apply(func, tuple(args))
            except Exception as ex:
                print "Exception during execution:", ex
                if not xid is None:
                    self.transport.send(packb([1, xid, None, "Error during execution:" + str(ex)]))
                continue

            if not xid is None:
                self.transport.send(packb([1, xid, result, None]))
                    

