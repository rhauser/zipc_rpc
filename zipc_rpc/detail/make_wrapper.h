// this is -*- c++ -*-
#ifndef ZIPC_RPC_DETAIL_MAKE_WRAPPER_H_
#define ZIPC_RPC_DETAIL_MAKE_WRAPPER_H_

namespace tdaq {

    namespace rpc {

        namespace detail {

            template<class RESULT, class FUNC>
            struct call {
                template<class...ARGS>
                RESULT operator()(FUNC func, ARGS...args) {
                    return func(std::forward<ARGS>(args)...); 
                }
            };
            
            template<class FUNC>
            struct call<void,FUNC> {
                
                template<class...ARGS>
                msgpack::type::nil_t operator()(FUNC func, ARGS...args) {
                    func(std::forward<ARGS>(args)...); 
                    return msgpack::type::nil_t();
                }
            };
            
            template<class FUNC, size_t...INDICES>
            std::function<msgpack::object (msgpack::zone&, const msgpack::object&)> 
            make_wrapper(FUNC func, std::index_sequence<INDICES...> is) {
                return [func, &is](msgpack::zone& zone, const msgpack::object& params) -> msgpack::object {
#if 0                    
                    if(params.via.array.size != is.size()) {
                        throw std::domain_error("Wrong number of arguments");
                    }
#endif
                    typedef typename detail::function_traits<FUNC>::return_type RESULT;
                    return msgpack::object(call<RESULT,FUNC>()(func,params.via.array.ptr[INDICES].as<typename std::remove_cv<typename std::remove_reference<typename detail::function_traits<FUNC>::template argument<INDICES>::type>::type>::type>()...), zone);
                };
            }

        }
    }
}

#endif // ZIPC_RPC_DETAIL_MAKE_WRAPPER_H_
