// this is -*- c++ -*-
#ifndef ZIPC_RCP_ADAPTOR_ISSUE_H_
#define ZIPC_RCP_ADAPTOR_ISSUE_H_

#include "ers/Context.h"
#include "ers/RemoteContext.h"
#include "ers/Issue.h"
#include "ers/IssueFactory.h"

#include <msgpack.hpp>

namespace msgpack {
    MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
        namespace adaptor {

            template<>
            struct pack<ers::Context> {
                template <typename Stream>
                packer<Stream>& operator()(msgpack::packer<Stream>& o, ers::Context const& v) const {

                    o.pack_array(11);

                    // in order of ers::RemoteProcessContext constructor arguments
                    o.pack(v.host_name());
                    o.pack(v.process_id());
                    o.pack(v.thread_id());
                    o.pack(v.cwd());
                    o.pack(v.user_id());
                    o.pack(v.user_name());
                    o.pack(v.application_name());

                    // in order of ers::RemoteContext constructor arguments, followed by RemoteProcessContext
                    o.pack(v.package_name());
                    o.pack(v.file_name());
                    o.pack(v.line_number());
                    o.pack(v.function_name());

                    return o;
                }
            };

            template<>
            struct pack<ers::Issue> {
                template <typename Stream>
                packer<Stream>& operator()(msgpack::packer<Stream>& o, ers::Issue const& v) const {

                    o.pack_array(7);

                    o.pack(v.context());

                    // apart from context in order of IssueFactor::create() method:
                    o.pack(v.get_class_name());                    
                    o.pack((ers::severity )v.severity());
                    o.pack(v.time_t());
                    o.pack(v.message());
                    o.pack(v.qualifiers());
                    o.pack(v.parameters());

                    // note: no chained issue yet.

                    return o;
                }
            };


            template <>
            struct as<ers::RemoteContext> {
                ers::RemoteContext operator()(msgpack::object const& o) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 11) throw msgpack::type_error();
                    ers::RemoteProcessContext process(o.via.array.ptr[0].as<std::string>(),
                                                      o.via.array.ptr[1].as<int>(),
                                                      o.via.array.ptr[2].as<int>(),
                                                      o.via.array.ptr[3].as<std::string>(),
                                                      o.via.array.ptr[4].as<int>(),
                                                      o.via.array.ptr[5].as<std::string>(),
                                                      o.via.array.ptr[6].as<std::string>());

                    return ers::RemoteContext(o.via.array.ptr[7].as<std::string>(),
                                              o.via.array.ptr[8].as<std::string>(),
                                              o.via.array.ptr[9].as<int>(),
                                              o.via.array.ptr[10].as<std::string>(),
                                              process);
                }
            };

            template <>
            struct as<ers::Issue*> {
                ers::Issue* operator()(msgpack::object const& o) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 7) throw msgpack::type_error();

                    ers::RemoteContext context = o.via.array.ptr[0].as<ers::RemoteContext>();

                    return ers::IssueFactory::instance().create(o.via.array.ptr[1].as<std::string>(),
                                                                context,
                                                                ers::Severity(o.via.array.ptr[2].as<ers::severity>()),
                                                                std::chrono::system_clock::time_point(std::chrono::microseconds(o.via.array.ptr[3].as<int>())),
                                                                o.via.array.ptr[4].as<std::string>(),
                                                                o.via.array.ptr[5].as<std::vector<std::string> >(),
                                                                o.via.array.ptr[6].as<std::map<std::string,std::string> >(),
                                                                nullptr);
                }
            };

        }
    }
}

MSGPACK_ADD_ENUM(ers::severity)

#endif // ZIPC_RCP_ADAPTOR_ISSUE_H_
