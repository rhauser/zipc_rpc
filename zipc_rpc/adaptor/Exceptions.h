// this is -*- c++ -*-
#ifndef ZIPC_RCP_ADAPTOR_EXCEPTIONS_H_
#define ZIPC_RCP_ADAPTOR_EXCEPTIONS_H_

#include <exception>
#include <stdexcept>

#include <new>
#include <memory>
#include <typeinfo>
#include <functional>
#include <system_error>
#include <future>
#include <regex>

#include <msgpack.hpp>

namespace msgpack {
    MSGPACK_API_VERSION_NAMESPACE(MSGPACK_DEFAULT_API_NS) {
        namespace adaptor {

            template<>
            struct pack<std::exception> {

                template <typename Stream>
                packer<Stream>& operator()(msgpack::packer<Stream>& o, std::exception const& v) const {

                    o.pack_array(2);

                    if(dynamic_cast<const std::invalid_argument*>(&v)) {
                        o.pack("invalid_argument");
                    } else if(dynamic_cast<const std::domain_error*>(&v)) {
                        o.pack("domain_error");
                    } else if(dynamic_cast<const std::length_error*>(&v)) {
                        o.pack("length_error");
                    } else if(dynamic_cast<const std::out_of_range*>(&v)) {
                        o.pack("out_of_range");
                    } else if(dynamic_cast<const std::future_error*>(&v)) {
                        o.pack("future_error");
                    } else if(dynamic_cast<const std::logic_error*>(&v)) {
                        o.pack("logic_error");
                    } else if(dynamic_cast<const std::range_error*>(&v)) {
                        o.pack("range_error");
                    } else if(dynamic_cast<const std::overflow_error*>(&v)) {
                        o.pack("overflow_error");
                    } else if(dynamic_cast<const std::underflow_error*>(&v)) {
                        o.pack("underflow_error");
                    } else if(dynamic_cast<const std::regex_error*>(&v)) {
                        o.pack("regex_error");
                    } else if(dynamic_cast<const std::system_error*>(&v)) {
                        o.pack("system_error");
                    } else if(dynamic_cast<const std::runtime_error*>(&v)) {
                        o.pack("runtime_error");
                    } else if(dynamic_cast<const std::bad_typeid*>(&v)) {
                        o.pack("bad_typeid");
                    } else if(dynamic_cast<const std::bad_cast*>(&v)) {
                        o.pack("bad_cast");
                    } else if(dynamic_cast<const std::bad_weak_ptr*>(&v)) {
                        o.pack("bad_weak_ptr");
                    } else if(dynamic_cast<const std::bad_function_call*>(&v)) {
                        o.pack("bad_function_call");
                    } else if(dynamic_cast<const std::bad_array_new_length*>(&v)) {
                        o.pack("bad_array_new_length");
                    } else if(dynamic_cast<const std::bad_alloc*>(&v)) {
                        o.pack("bad_alloc");
                    } else if(dynamic_cast<const std::bad_exception*>(&v)) {
                        o.pack("bad_exception");
                    } else {
                        o.pack("UNKOWN_STDEXCEPTION");
                    }
                    o.pack(v.what());
                    return o;
                }
            };

            template <>
            struct as<std::exception_ptr> {
                std::exception_ptr operator()(msgpack::object const& o) const {
                    if (o.type != msgpack::type::ARRAY) throw msgpack::type_error();
                    if (o.via.array.size != 2) throw msgpack::type_error();
                    
                    std::string type = o.via.array.ptr[0].as<std::string>();
                    std::string msg  = o.via.array.ptr[1].as<std::string>();
                    
                    if(type == "logic_error") {
                        return std::make_exception_ptr(std::logic_error(msg));
                    } else if(type == "invalid_argument") {
                        return std::make_exception_ptr(std::invalid_argument(msg));
                    } else if(type == "domain_error") {
                        return std::make_exception_ptr(std::domain_error(msg));
                    } else if(type == "length_error") {
                        return std::make_exception_ptr(std::length_error(msg));
                    } else if(type == "out_of_range") {
                        return std::make_exception_ptr(std::out_of_range(msg));
#if 0
                    } else if(type == "future_error") {
                        // FIXME: needs error code
                        return std::make_exception_ptr(std::future_error());
#endif
                    } else if(type == "runtime_error") {
                        return std::make_exception_ptr(std::runtime_error(msg));
                    } else if(type == "range_error") {
                        return std::make_exception_ptr(std::range_error(msg));
                    } else if(type == "overflow_error") {
                        return std::make_exception_ptr(std::overflow_error(msg));
                    } else if(type == "underflow_error") {
                        return std::make_exception_ptr(std::underflow_error(msg));
#if 0                        
                    } else if(type == "regex_error") {
                        // FIXME: needs error code
                        return std::make_exception_ptr(std::regex_error(msg));
#endif
                    } else if(type == "system_error") {
                        // FIXME: needs error code
                        return std::make_exception_ptr(std::system_error());
                    } else if(type == "bad_typeid") {
                        return std::make_exception_ptr(std::bad_typeid());
                    } else if(type == "bad_cast") {
                        return std::make_exception_ptr(std::bad_cast());
                    } else if(type == "bad_weak_ptr") {
                        return std::make_exception_ptr(std::bad_weak_ptr());
                    } else if(type == "bad_function_call") {
                        return std::make_exception_ptr(std::bad_function_call());
                    } else if(type == "bad_alloc") {
                        return std::make_exception_ptr(std::bad_alloc());
                    } else if(type == "bad_array_new_length") {
                        return std::make_exception_ptr(std::bad_array_new_length());
                    } else if(type == "bad_exception") {
                        return std::make_exception_ptr(std::bad_exception());
                    } 
                    
                    // unknown exception
                    return std::make_exception_ptr(std::exception());
                }
            };
            
        }
    }
}


#endif // ZIPC_RCP_ADAPTOR_EXCEPTIONS_H_

