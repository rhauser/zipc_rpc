// this is -*- C++ -*-
#ifndef ZIPC_RPC_SERVER_H_
#define ZIPC_RPC_SERVER_H_

#include <exception>
#include <memory>
#include <string>
#include <functional>
#include <type_traits>
#include <utility>

#include "ers/Issue.h"
#include "ipc/Server.hpp"

#include "msgpack.hpp"
#include "zipc_rpc/adaptor/Exceptions.h"
#include "zipc_rpc/adaptor/Issue.h"
#include "zipc_rpc/detail/function_traits.h"
#include "zipc_rpc/detail/make_wrapper.h"

namespace tdaq {

    namespace rpc {
        
        class BaseServer : public tdaq::ipc::Server {
        protected:

            template<class HANDLER>
            BaseServer(HANDLER handler, zmq::socket_type type = zmq::socket_type::rep, int threads = 1, zmq::context_t& context = tdaq::ipc::Context::instance())
                : tdaq::ipc::Server(type, handler, threads, context)
            {}

            virtual ~BaseServer() {}

            BaseServer(const Server& ) = delete;
            BaseServer& operator=(const Server& ) = delete;

        public: 

            template<class FUNC>
            void add_method(const std::string& name, FUNC method) {
                m_methods[name] = detail::make_wrapper(method, std::make_index_sequence<detail::function_traits<FUNC>::arity>());
            }

            template<class C, class R, class...PARAMS, class...ARGS>
            void add_method(const std::string& name, R(C::*method)(PARAMS...), C* _this, ARGS...args)
            {
                add_method(name, std::function<R(PARAMS...)>(std::bind(method, _this, std::forward<ARGS>(args)...)));
            }

            void remove_method(const std::string& name)
            {
                auto it = m_methods.find(name);
                if(it != m_methods.end()) {
                    m_methods.erase(it);
                }
            }
        
            bool has_method(const std::string& name) const
            {
                return m_methods.find(name) != m_methods.end();
            }

        protected:
            
            //
            // To override by user if he wants to be informed of certain errors and/or handle them
            //
            virtual void invalid_format() {}
            virtual void invalid_method(const std::string& /* method */) {}
            virtual void exception_caught(const std::string& /* method */, const std::exception& /* ex */) {}
        
            msgpack::object dispatch(msgpack::zone& zone, const std::string& method, const msgpack::object& params)
            {
                if(has_method(method)) {
                    return m_methods[method](zone, params);
                } else if(method == "__list") {
                    std::vector<std::string> result;
                    result.reserve(m_methods.size());
                    for(auto& elem : m_methods) {
                        result.push_back(elem.first);
                    }
                    return msgpack::object(result, zone);
                } else if(method == "__ping") {
                    return msgpack::object(true, zone);
                } else {
                    invalid_method(method);
                    throw std::domain_error("No such method");
                }
            }
        
        private:
            std::map<std::string, std::function<msgpack::object (msgpack::zone&, const msgpack::object&)>> m_methods;
        };

        class Server : public BaseServer {
        public:
            Server(zmq::socket_type type = zmq::socket_type::rep, int threads = 1,zmq::context_t& context = tdaq::ipc::Context::instance())
                : BaseServer(std::function<zmq::message_t (zmq::message_t& )>(std::bind(&Server::handler, this, std::placeholders::_1)), type, threads, context)
            {}

        private:
            zmq::message_t handler(zmq::message_t &msg)
            {
                msgpack::object_handle oh = msgpack::unpack(msg.data<const char>(), msg.size());
                msgpack::object request = oh.get();

                if(!(request.type == msgpack::type::ARRAY) ||
                   !(request.via.array.size == 4) ||
                   !(request.via.array.ptr[0].as<uint32_t>() == 0)) {
                    invalid_format();
                    zmq::message_t reply;
                    return reply;
                }

                uint32_t xid = request.via.array.ptr[1].as<uint32_t>();
                std::string method = request.via.array.ptr[2].as<std::string>();
                
                msgpack::sbuffer buffer;                
                msgpack::packer<msgpack::sbuffer>  packer(&buffer);
                
                packer.pack_array(4);
                packer.pack(1);
                packer.pack(xid);
                
                try {
                    msgpack::zone zone;
                    msgpack::object result = dispatch(zone, method, request.via.array.ptr[3].as<msgpack::object>());
                    
                    packer.pack(result);
                    packer.pack_nil();
                    
                  } catch(ers::Issue& ex) {
                    exception_caught(method, ex);
                    
                    packer.pack_nil();

                    packer.pack_array(2);
                    packer.pack(1);
                    packer.pack(ex);
                    
                 } catch(std::exception& ex) {

                    exception_caught(method, ex);
                    
                    packer.pack_nil();

                    packer.pack_array(2);
                    packer.pack(0);
                    packer.pack(ex);
                    
                } catch(...) {
                    exception_caught(method, std::exception());
                    
                    packer.pack_nil();

                    packer.pack_array(2);
                    packer.pack(2);
                    packer.pack("unkown_exception");
                }

                zmq::message_t reply(buffer.size());
                memcpy(reply.data(), buffer.data(), buffer.size());
                return reply;

            }

        };

        class OnewayServer : public BaseServer {
        public:
            OnewayServer(zmq::socket_type type = zmq::socket_type::router, int threads = 1,zmq::context_t& context = tdaq::ipc::Context::instance())
                : BaseServer(std::function<void (zmq::message_t& )>(std::bind(&OnewayServer::handler, this, std::placeholders::_1)), type, threads, context)
            {}

        private:
            void handler(zmq::message_t &msg)
            {
                msgpack::object_handle oh = msgpack::unpack(msg.data<const char>(), msg.size());
                msgpack::object request = oh.get();

                if(!(request.type == msgpack::type::ARRAY) ||
                   !(request.via.array.size == 3) ||
                   !(request.via.array.ptr[0].as<uint32_t>() == 2)) {
                    invalid_format();
                    return;
                }

                std::string method = request.via.array.ptr[1].as<std::string>();
                
                try {
                    msgpack::zone zone;
                    msgpack::object result = dispatch(zone, method, request.via.array.ptr[2].as<msgpack::object>());
                    (void )result;
                } catch(std::exception& ex) {
                    exception_caught(method, ex);
                } catch(...) {
                    exception_caught(method, std::exception());
                }
            }
        };

    }
}

#endif // ZIPC_RPC_SERVER_H_
