// this is -*- c++ -*-
#ifndef ZIPC_RPC_CLIENT_H_
#define ZIPC_RPC_CLIENT_H_

#include "msgpack.hpp"
#include "ipc/Client.hpp"
#include "ipc/PublicClient.hpp"
#include "zipc_rpc/adaptor/Exceptions.h"
#include "zipc_rpc/adaptor/Issue.h"

#include <future>

namespace tdaq {
    namespace rpc {

        namespace detail {
            template<class RESULT>
            struct reply_unpacker {
                RESULT operator()(msgpack::object& result) {
                    return result.as<RESULT>();
                }
            };
            
            template<>
            struct reply_unpacker<void> {
                void operator()(msgpack::object& ) {}
            };
        }


        template<class T>
        class BaseClient : public T {
        public:

            BaseClient(::zmq::socket_type type, tdaq::ipc::Endpoint const& endpoint,
                       ::zmq::context_t& context = tdaq::ipc::Context::instance())
                : T(type, endpoint, context)
            {}


            BaseClient(::zmq::socket_type type, tdaq::ipc::Partition const& partition,
                       std::string const& name, ::zmq::context_t& context =  tdaq::ipc::Context::instance())
                : T(type, partition, name, context)
            {}

        private:
            template<class...ARGS>
            void send_request(bool request, const std::string& method, ARGS...args)
            {
                msgpack::sbuffer buf;
                msgpack::packer<msgpack::sbuffer>  packer(&buf);

                packer.pack_array(request ? 4 : 3);
                packer.pack(request ? 0 : 2);
                if(request) {
                    packer.pack(m_xid++);
                }
                packer.pack(method);
                packer.pack(msgpack::type::make_tuple(std::forward<ARGS>(args)...));

                zmq::message_t request_msg(buf.size());
                memcpy(request_msg.data(), buf.data(), buf.size());
                this->send(request_msg);
            }

            template<class RESULT>
            RESULT recv_reply()
            {
                zmq::message_t reply;
                this->recv(reply);
        
                msgpack::object_handle result;
                msgpack::unpack(result, reply.data<const char>(), reply.size());
        
                msgpack::object oh(result.get());

                if(!oh.via.array.ptr[3].is_nil()) {
                    if(oh.via.array.ptr[3].via.array.ptr[0] == 0) {
                        // std::exception
                        std::exception_ptr ex_ptr = oh.via.array.ptr[3].via.array.ptr[1].as<std::exception_ptr>();
                        std::rethrow_exception(ex_ptr);

                    } else if(oh.via.array.ptr[3].via.array.ptr[0] == 1) {
                        // ers::Issue
                        std::unique_ptr<ers::Issue> issue(oh.via.array.ptr[3].via.array.ptr[1].as<ers::Issue*>());
                        issue->raise();
                    } else {
                        // unknown exception type
                        throw std::exception();
                    }
                }

                detail::reply_unpacker<RESULT> ret_value;
                return ret_value(oh.via.array.ptr[2]);
            }

        public:
            template<class RESULT, class...ARGS>
            RESULT call(const std::string& method, ARGS...args)
            {
                send_request(true, method, std::forward<ARGS>(args)...);
                return recv_reply<RESULT>();
            }

            template<class RESULT, class...ARGS>
            std::future<RESULT> async_call(const std::string& method, ARGS...args)
            {
                send_request(true, method, std::forward<ARGS>(args)...);
                return std::async(std::launch::async, std::bind(&BaseClient::recv_reply<RESULT>, this));
            }

            template<class...ARGS>
            void notify(const std::string& method, ARGS...args)
            {
                send_request(false, method, std::forward<ARGS>(args)...);
            }

        private:
            uint32_t m_xid = 0;
        };

        typedef BaseClient<tdaq::ipc::Client> Client;
        typedef BaseClient<tdaq::ipc::PublicClient> PublicClient;

    }
}

#endif // ZIPC_RPC_CLIENT_H_
